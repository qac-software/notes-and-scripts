#!/bin/bash

echo "Installing build agent..."
sudo useradd teamcity || true
mkdir /home/gpowers/.teamcity || true
cd /home/gpowers/.teamcity
wget http://localhost:8111/update/buildAgent.zip
sudo unzip buildAgent.zip -d buildAgent
cd buildAgent/bin
sudo chmod +x agent.sh
cd ../conf
sudo cp buildAgent.dist.properties buildAgent.properties
sudo chown -R teamcity:teamcity /home/gpowers/.teamcity

echo "Install complete. Setting up service..."
# download service file to /usr/lib/systemd/system/teamcity_agent.service
sudo mkdir /usr/lib/systemd/system || true
sudo wget -O /usr/lib/systemd/system/teamcity_agent.service https://bitbucket.org/qac-software/notes-and-scripts/raw/master/install-teamcity-linux/teamcity_agent.service
sudo systemctl enable teamcity_agent.service

echo "Service installed to /usr/lib/systemd/system/teamcity_agent.service"
echo ""
echo "Installation is complete, and the agent service will run on system start."
echo "Before starting the agent via a system service, it's recommended to start it once manually and let it upgrade."
echo "More information and instructions are available in the README:"
echo "https://bitbucket.org/qac-software/notes-and-scripts/raw/master/install-teamcity-linux/README.md"
