#!/bin/bash

TC_VERSION="2017.2.2"

cd /opt
sudo wget https://download-cf.jetbrains.com/teamcity/TeamCity-$TC_VERSION.tar.gz
sudo tar -xzvf TeamCity-$TC_VERSION.tar.gz
sudo useradd teamcity
sudo chown -R teamcity:teamcity /opt/TeamCity

# no idea why we have to do this next bit, but sometimes build agents fail without it.
sudo mkdir /home/teamcity/ || true
sudo chown -R teamcity:teamcity /home/teamcity

echo "TeamCity installed to /opt/TeamCity. Installing service..."

# download service file to /usr/lib/systemd/system/teamcity.service
sudo mkdir /usr/lib/systemd/system || true
sudo wget -O /usr/lib/systemd/system/teamcity.service https://bitbucket.org/qac-software/notes-and-scripts/raw/master/install-teamcity-linux/teamcity.service
sudo systemctl enable teamcity.service

echo "Service installed to /usr/lib/systemd/system/teamcity.service"
echo ""
echo "Installation is complete, and the TeamCity service will run on system start."
echo "To manually start the service now, run 'sudo systemctl start teamcity.service'."
echo "Don't forget to set up a build agent once TeamCity setup is complete."
