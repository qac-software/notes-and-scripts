# TeamCity Install Scripts for Ubuntu Server

## Requirements:
- systemd based OS (should be default on latest ubuntu versions)
- java (TeamCity is a Java program) `apt install default-jre`
- wget (for downloading scripts -- should be installed by default)
- unzip (for unzipping agent .zip) `apt install unzip`

## Notes:
- The scripts and service files provided in this directory are written with hard-coded paths assuming that you're logged in as a user named `gpowers`.

### Install TeamCity Server
(from home dir)

- Run `wget https://bitbucket.org/qac-software/notes-and-scripts/raw/master/install-teamcity-linux/install_teamcity.sh`

- Run `chmod +x install_teamcity.sh`

- Run `./install_teamcity.sh`

- *NOTE:* If planning to use TeamCity with docker (as in having TC send docker commands, start containers, etc), add TC to the docker usergroup. Run `sudo usermod -aG docker teamcity`. Do this before starting the server or any build agents.

- To start the server, run `sudo systemctl start teamcity.service`

- The server will be started automatically on system startup going forward.

### Install a build agent
(after TC server is installed)
(from home dir)

- Run `wget https://bitbucket.org/qac-software/notes-and-scripts/raw/master/install-teamcity-linux/install_agent.sh`

- Run `chmod +x install_agent.sh`

- Run `./install_agent.sh`

- We want to run the agent manually at first to give it time to upgrade. If it's started by the service, the upgrade process might fail. Run `sudo -H -u teamcity ~/.teamcity/buildAgent/bin/agent.sh run`.

- Once the upgrade from the previous step is complete, kill the process and run `sudo systemctl start teamcity_agent.service`

- The agent will be started automatically on system startup going forward.
