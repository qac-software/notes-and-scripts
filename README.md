# QAC Portal Server - Notes and Scripts

Various pieces of information regarding the portal server.

## What's here:

### reproducing-server-env.md

    Directions on setting up a server VM in the same way it was done the first time.

### install-teamcity-linux/

    Directions (README) and scripts for installing TeamCity.

### server-api-docs/

    Documentation page for using the portal server API - useful for writing front-end pages, or other frameworks that want to utilize the portal's tools programatically.

### dockerfiles/

    Directory containing various Dockerfiles used in building tools and the server.

### configuring-node-server/

    Documentation (README) on the portal node.js server configuration file.

### server-directories/

    Documentation (README) on the role, structure, and behavior of the portal server's temporary tool instance directories.
