# Configuring the QAC tool portal node.js server 

The server configuration file is written as a JSON file, and passed as the first argument to the node process.

```js
node index.js /path/to/config/file.json
```

## Config Keys

#### `TOOL_EXES` <Object>

Keys: Tools the server should offer endpoints for. Each key should correspond to the name of the 

Values: Objects defining configuration options for each tool. Configuration options are as follows:

- `command`: Name of the process to start, for example `java` or `node`. By default, uses executables found in system $PATH by name, but this property can also be set to an absolute path to the executable.

- `executablePreArgs`: arguments/flags to pass to process BEFORE executable program name/path. For example, java needs a `-jar` flag when we want to run a jar file. 

- `executableWorkingDir`: Directory that executable gets started from; executable's cwd. If none is passed, we use the directory where the executable in question lives.

- `executableFullPath`: Full path to the tool executable. In the case of tools written in Java, this is often a `.jar` file.

- `argsOverride`: Overrides the rest of the arguments listed above. Array of string arguments to send to spawned process. The argsOverride array should follow the same rules as the args array passed to node's [child_process.spawn()](https://nodejs.org/api/child_process.html#child_process_child_process_spawn_command_args_options).

**NOTE**: When spawning a tool process, the path to the generated tool run `config.toml` file will be appended as the final argument to the process, after all of the arguments listed above (and after the provided argsOverride array, if that's used instead).

#### `SERVER_IP` <String>

IP for the server to run at. For localhost with public access as well as loopback support, use `0.0.0.0`.

#### `SERVER_PORT` <Number>

Port for the server to run on. `8080` is common.

#### `SERVER_DIR_LOGS` <String>

Directory in which the server should write all log files.

#### `SERVER_DIR_FRONTEND` <String>

Directory where the server frontend directory lives. This is the directory that's output by the frontend repo's `build_prod` npm command. 

#### `SERVER_DIR_TOOL_INSTANCE_HOME` <String>

Directory where the server should create all of its temporary tool instance directories (see [Instance Directories](https://bitbucket.org/qac-software/notes-and-scripts/src/master/server-directories/README.md) document for more information)

#### `langDiffIgnoreDir` <String>

Directory which holds all ignore files for the language diff tool. These files will be read by each instance of the language diff tool, as well as the server's "ignoreUpdater.html" page. Default ignore files can be found [here](https://bitbucket.org/qac-software/language-diff-tool/src/master/ignorefiles/). The contents of the directory provided to langDiffIgnoreDir should be essentially the same as what's shown at this link.

## Docker

In a configuration where the server is to start Docker containers for its tool executions, it's recommended to use exclusively the `argsOverride` option, as Docker processes often require very unique arguments, most of which can't be covered by the other process configuration options offered here (such as mounting directories to container volumes).

## Example File

The server is intended to be used in a Docker setup (both running inside of Docker, and using Docker to run tools), so we'll show an example of this configuration:

```json
{
    "TOOL_EXES": {
        "LINK_CHECKER": {
            "argsOverride": [
                "docker",
                "run",
                "-i",
                "--mount",
                "source=server-tool-instance-home,target=/qac_server_tool_instance_home",
                "qaconsultants/tool-link-checker"
            ]
        },
        "LANGUAGE_DIFF": {
            "argsOverride": [
                "docker",
                "run",
                "-i",
                "--mount",
                "source=server-tool-instance-home,target=/qac_server_tool_instance_home",
                "--mount",
                "type=bind,source=/home/gpowers/server-data/tool_data/lang_diff/ignorefiles,target=/server_ignorefiles",
                "qaconsultants/tool-lang-diff"
            ]
        }
    },
    "SERVER_IP": "0.0.0.0",
    "SERVER_PORT": 8080,
    "SERVER_DIR_LOGS": "/qac_server_data/logs",
    "SERVER_DIR_FRONTEND": "/qac_server_frontend",
    "SERVER_DIR_TOOL_INSTANCE_HOME": "/qac_server_tool_instance_home",
    "langDiffIgnoreDir": "/qac_server_data/tool_data/lang_diff/ignorefiles"
}
```

Note that the directories listed at the bottom (`SERVER_DIR_LOGS`, `SERVER_DIR_FRONTEND`, `SERVER_DIR_TOOL_INSTANCE_HOME`, `langDiffIgnoreDir`) are all relative to a volume we've mounted when starting the node server docker container. Examples of the setup and mounting process can be found in the [reproducing server environment](https://bitbucket.org/qac-software/notes-and-scripts/src/master/reproducing-server-env.md) document inside the "Create projects" section.
