#!/bin/bash

echo "Installing service..."
cd ~
mkdir portalserv_tmp
cd portalserv_tmp

# download and move start script
wget https://bitbucket.org/qac-software/notes-and-scripts/raw/master/install-portal-server-service/start_server_container.sh
mv start_server_container.sh ~
chmod +x ~/start_server_container.sh

# download and install service
sudo wget -O /usr/lib/systemd/system/portal_server.service https://bitbucket.org/qac-software/notes-and-scripts/raw/master/install-portal-server-service/portal_server.service
sudo systemctl enable portal_server.service

cd ~
rm -r portalserv_tmp

echo "Service installed to /usr/lib/systemd/system/portal_server.service"
echo ""
echo "Installation is complete, and the service will run on system start."

echo "More information and instructions are available in the README:"
echo "https://bitbucket.org/qac-software/notes-and-scripts/"
