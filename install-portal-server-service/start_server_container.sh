#!/bin/bash

# this script is run by a systemd service that runs on boot
# delay startup because it doesn't work otherwise
echo "waiting 2 minutes for TeamCity to finish starting..."
sleep 2m
echo "Running docker container restart for portal server"
docker container restart qac_portal_server

