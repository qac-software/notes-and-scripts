# Steps to reproduce QAC Portal Server environment

## Prereqs

### VM running systemd-based version of ubuntu server

## Assumptions

### - Ubuntu version 16.04

### - Main user account on ubuntu server is named `gpowers`

## Ubuntu Server

### run `sudo apt update && sudo apt upgrade`

### install java

    sudo apt install default-jre

### install docker

    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    
    sudo apt update
    
    # make sure the output of the following command points to
    # the download.docker.com repo instead of the default ubuntu repo
    # (check version table in output)
    apt-cache policy docker-ce
    sudo apt install -y docker-ce
    sudo usermod -aG docker $USER
    echo "$USER added to docker user group. You will have to log out and back in before using docker commands without sudo."

### install unzip

    sudo apt install unzip

### install nodejs (install of 9.x.x shown here, adjust version if needed)

    curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash -
    sudo apt install -y nodejs

### Create server data directory
    
    From /home/gpowers ... (NOTE: USERNAME 'gpowers' IS ASSUMED)
    - run `mkdir server-data; cd server-data`
    - run `mkdir config logs portal-frontend tool_data`
    - For some reason, sometimes TC/docker don't like the default permissions on this dir and can't properly write to it, even with user groups configured properly. Since this is an internal project and nobody's touching this machine except server admin, we'll cheat;
    - run `sudo chmod 777 /home/gpowers/server-data/`

### Download Language Diff Tool ignore files

    From /home/gpowers ...
    - run `cd server-data/tool_data`
    - run `mkdir lang_diff; cd lang_diff`
    - run `mkdir ignorefiles; cd ignorefiles`
    - run `for DL_LANG in {arabic_,brazilianportuguese_,chinese_,french_,german_,italian_,japanese_,korean_,portuguese_,russian_,spanish_,}ignore.txt; do wget https://bitbucket.org/qac-software/language-diff-tool/raw/master/ignorefiles/$DL_LANG; done`

### Create server config file

File goes in `/home/gpowers/server-data/config/server_config_file.json`. Create dir if it doesn't exist.
Any extra tools that aren't shown here will need to be added. Please update this document if/when that happens!

    {
        "TOOL_EXES": {
            "LINK_CHECKER": {
                "argsOverride": [
                    "docker",
                    "run",
                    "-i",
                    "--mount",
                    "source=server-tool-instance-home,target=/qac_server_tool_instance_home",
                    "qaconsultants/tool-link-checker"
                ]
            },
            "LANGUAGE_DIFF": {
                "argsOverride": [
                    "docker",
                    "run",
                    "-i",
                    "--mount",
                    "source=server-tool-instance-home,target=/qac_server_tool_instance_home",
                    "--mount",
                    "type=bind,source=/home/gpowers/server-data/tool_data/lang_diff/ignorefiles,target=/server_ignorefiles",
                    "qaconsultants/tool-lang-diff"
                ]
            }
        },
        "SERVER_IP": "0.0.0.0",
        "SERVER_PORT": 8080,
        "SERVER_DIR_LOGS": "/qac_server_data/logs",
        "SERVER_DIR_FRONTEND": "/qac_server_frontend",
        "SERVER_DIR_TOOL_INSTANCE_HOME": "/qac_server_tool_instance_home",
        "langDiffIgnoreDir": "/qac_server_data/tool_data/lang_diff/ignorefiles",
        "SERVER_DIR_TOOL_DOWNLOAD_HOME": "/qac_server_data/downloads"
    }

### Docker setup

Create volume server-tool-instance-home

    docker volume create server-tool-instance-home

## TeamCity CI

### install teamcity

1.  Use instructions at <https://bitbucket.org/qac-software/notes-and-scripts/src/master/install-teamcity-linux/README.md>

2.  Add TeamCity to docker user group, or else build agents won't be able to talk to the docker socket

    sudo usermod -aG docker teamcity

### TeamCity Global Config

1. In TeamCity, click 'Administration' in the top right  corner, and then "Global Settings" in the left sidebar (under Server Administration header)

2. In the 'Version Control Settings' section, set the following options:

    - Default VCS changes check interval: 300 seconds

    - Default VCS trigger quiet period: 120 seconds

### Create projects

1.  NOTE: when creating projects that use BitBucket, TeamCity will show an "Add Connection" pop-up window;

    - The window will ask for a key and secret from BitBucket. For the portal server, we use one of the OAuth consumers named "PRODPortalTeamCity" or "QACPortalTeamCity" (depending on if this is on the production or dev machine) from this page: <https://bitbucket.org/account/user/qac-software/api>
    - The key and secret here can be re-used for as many projects as needed.
    - The URL for this consumer will need to change if the TeamCity server is hosted from a different IP.

2.  Create the following projects and build configurations:

#### 1. Cleanup Build Configuration - Create using 'manually' option (no repository involved)
    
    Project description: Clean up docker containers and images

    Once Cleanup project is created, click 'Create build configuration' on Cleanup project settings page. Name it 'Clean'.

    On the "New VCS Root" page, click 'skip' at the bottom of the page.

    For each step below, click "build steps" in the sidebar if not already on the build steps page, then "add build step".

        1.  Build Step 1: Stop Containers
        
            Runner Type: command line
            
                docker stop $(docker ps -a -q)
        
        2.  Build Step 2: Prune Containers
        
            Runner Type: command line
            
                docker container prune -f
        
        3.  Build Step 3: Prune Images
        
            Runner Type: command line
            
                docker image prune -f
        
        4.  Build Step 4: Start Server Container
        
            Runner Type: command line
            NOTE: This should be the same script as used in the Portal Server > Docker Run step.

                docker run -d -p 8080:8080 \
                --name qac_portal_server \
                --mount type=bind,source=/var/run/docker.sock,target=/var/run/docker.sock \
                --mount type=bind,source=/home/gpowers/server-data/,target=/qac_server_data \
                --mount type=bind,source=/home/gpowers/server-data/portal-frontend,target=/qac_server_frontend \
                --mount source=server-tool-instance-home,target=/qac_server_tool_instance_home \
                qaconsultants/qac-portal-server \
                /qac_server_data/config/server_config_file.json

#### 2. Portal Server Build Configuration (From BitBucket Cloud repo; 'qac-software/portal-server' repo)

    Cancel 'scanning VCS repositories' auto-build step finder when configuration is created, configure build steps manually.
    
        1.  Build Step 1: Docker Build
        
            1.  Use Docker Build runner type
            
            2.  Set Path to file to "Dockerfile" (no directory)
            
            3.  set Name:tag(s) to `qaconsultants/qac-portal-server`

            4.  Leave 'additional arguments for build command' as '--pull
        
        2.  Build Step 2: Stop Running Docker
        
            Runner Type: command line
            
                # stop container (exit 'true' if failed so we don't kill the CI build),
                # then remove container and again exit true if fail.
                
                docker container stop qac_portal_server || true && \
                docker container rm qac_portal_server || true
        
        3.  Build Step 3: Docker Run
        
            Runner type: command line
            
                docker run -d -p 8080:8080 \
                --name qac_portal_server \
                --mount type=bind,source=/var/run/docker.sock,target=/var/run/docker.sock \
                --mount type=bind,source=/home/gpowers/server-data/,target=/qac_server_data \
                --mount type=bind,source=/home/gpowers/server-data/portal-frontend,target=/qac_server_frontend \
                --mount source=server-tool-instance-home,target=/qac_server_tool_instance_home \
                qaconsultants/qac-portal-server \
                /qac_server_data/config/server_config_file.json

    Portal Server Build: Version Control Settings

    Go to Portal Server Project > Configuration named "Build" > Version Control Settings

    Click on the existing VCS root configuration in the list to edit it.

    In the VCS Root Name field, at the end of the text, change the existing "#refs/heads/master" to "#refs/heads/server_prod" (both without quotes)

    In the "Default Branch" field (under General Settings in this same page), change "/refs/heads/master" to "/refs/heads/server_prod" (both without quotes)

    Click save at the bottom of the page.

    
#### 3. Portal Frontend Build Configuration (From BitBucket Cloud repo; 'qac-software/portal-frontend' repo)
    
    Cancel 'scanning VCS repositories' auto-build step finder when configuration is created, configure build steps manually.

        1.  Build Step 1: Install Dependencies

            - Use Command Line runner type

            - Script: 
        
                npm install
        
        2.  Build Step 2: Build

            - Use Command Line runner type

            - Script: 
        
                npm run build_prod /home/gpowers/server-data/portal-frontend
        
        3.  Build Step 3: Restart Server Container
        
            This is required for some reason. Server doesn't see changes in frontend directory unless it's restarted.

            - Use Command Line runner type

            - Script: 
            
                docker container restart qac_portal_server

    Portal Frontend Build: Version Control Settings

    Go to Portal Frontend Project > Configuration named "Build" > Version Control Settings

    Click on the existing VCS root configuration in the list to edit it.

    In the VCS Root Name field, at the end of the text, change the existing "#refs/heads/master" to "#refs/heads/server_prod" (both without quotes)

    In the "Default Branch" field (under General Settings in this same page), change "/refs/heads/master" to "/refs/heads/server_prod" (both without quotes)

    Click save at the bottom of the page.

    
#### 4. Website Link Checker Tool Build Configuration (From BitBucket Cloud repo; 'qac-software/website-link-checker' repo)

    Cancel 'scanning VCS repositories' auto-build step finder when configuration is created, configure build steps manually.
    
        1.  Build Step: Docker Build
        
            1.  Use Docker Build runner type
            
            2.  Set Path to file to `Dockerfile` (no directory)
            
            3.  Set Name:tag(s) to `qaconsultants/tool-link-checker`
            
            4.  Set 'additional arguments for build command' to '--pull --network host'

    Link Checker Build: Version Control Settings

    Go to Link Checker Project > Configuration named "Build" > Version Control Settings

    Click on the existing VCS root configuration in the list to edit it.

    In the VCS Root Name field, at the end of the text, change the existing "#refs/heads/master" to "#refs/heads/server_prod" (both without quotes)

    In the "Default Branch" field (under General Settings in this same page), change "/refs/heads/master" to "/refs/heads/server_prod" (both without quotes)

    Click save at the bottom of the page.
    
#### 5.  Website Language Diff Tool Build Configuration

Cancel 'scanning VCS repositories' auto-build step finder when configuration is created, configure build steps manually.

    1.  Build Step: Docker Build

        1.  Use Docker Build runner type

        2.  Set Path to file to `Dockerfile` (no directory)

        3.  Set Name:tag(s) to `qaconsultants/tool-lang-diff`

            4.  Set 'additional arguments for build command' to '--pull --network host'

    Language Diff Build: Version Control Settings

    Go to Language Diff Project > Configuration named "Build" > Version Control Settings

    Click on the existing VCS root configuration in the list to edit it.

    In the VCS Root Name field, at the end of the text, change the existing "#refs/heads/master" to "#refs/heads/server_prod" (both without quotes)

    In the "Default Branch" field (under General Settings in this same page), change "/refs/heads/master" to "/refs/heads/server_prod" (both without quotes)

    Click save at the bottom of the page.

### Initially build projects

Manually click 'run' on the items in the TeamCity main view in the following order:

    - Portal server

    - Portal frontend

    - Link checker

    - Language diff tool

### Set up scheduled runs

#### Cleanup scheduled run

1. Navigate to 'Cleanup' Project > 'Clean' Build configuration > Edit Configuration Settings (top right of page)

2. Click 'Triggers' in the left side bar, and click 'Add New Trigger'. Choose 'Schedule Trigger' from the drop-down.

3. Configure the trigger as follows:

    When: Daily
    Time: 06:00
    Trigger only if there are pending changes: NOT CHECKED

4. Save the trigger.

#### Language Diff Tool Scheduled Run

1. Navigate to 'Language Diff Tool' Project > 'Build' Build configuration > Edit Configuration Settings (top right of page)

2. Click 'Triggers' in the left side bar

3. There should be one row in the table showing a trigger called 'VCS Trigger'. Click the drop-down menu option in the right-most column of this row, and select 'Disable trigger'.

4. Click 'Add New Trigger'. Choose 'Schedule Trigger' from the drop-down.

3. Configure the trigger as follows:

    When: Daily
    Time: 05:00
    Trigger only if there are pending changes: leave checked.

4. Save the trigger.

#### Link Checker Tool Scheduled Run (Same steps as Language Diff Tool steps above)

1. Navigate to 'Link Checker Tool' Project > 'Build' Build configuration > Edit Configuration Settings (top right of page)

2. Click 'Triggers' in the left side bar

3. There should be one row in the table showing a trigger called 'VCS Trigger'. Click the drop-down menu option in the right-most column of this row, and select 'Disable trigger'.

4. Click 'Add New Trigger'. Choose 'Schedule Trigger' from the drop-down.

3. Configure the trigger as follows:

    When: Daily
    Time: 05:00
    Trigger only if there are pending changes: leave checked.

4. Save the trigger.

#### Portal Frontend Scheduled Run (Same steps as Language Diff Tool steps above)

1. Navigate to 'Portal Frontend' Project > 'Build' Build configuration > Edit Configuration Settings (top right of page)

2. Click 'Triggers' in the left side bar

3. There should be one row in the table showing a trigger called 'VCS Trigger'. Click the drop-down menu option in the right-most column of this row, and select 'Disable trigger'.

4. Click 'Add New Trigger'. Choose 'Schedule Trigger' from the drop-down.

3. Configure the trigger as follows:

    When: Daily
    Time: 05:00
    Trigger only if there are pending changes: leave checked.

4. Save the trigger.

#### Portal Server Scheduled Run (Same steps as Language Diff Tool steps above)

1. Navigate to 'Portal Server' Project > 'Build' Build configuration > Edit Configuration Settings (top right of page)

2. Click 'Triggers' in the left side bar

3. There should be one row in the table showing a trigger called 'VCS Trigger'. Click the drop-down menu option in the right-most column of this row, and select 'Disable trigger'.

4. Click 'Add New Trigger'. Choose 'Schedule Trigger' from the drop-down.

3. Configure the trigger as follows:

    When: Daily
    Time: 05:00
    Trigger only if there are pending changes: leave checked.

4. Save the trigger.

#### Portal Frontend Scheduled Run (Same steps as Language Diff Tool steps above)

1. Navigate to 'Portal Server' Project > 'Build' Build configuration > Edit Configuration Settings (top right of page)

2. Click 'Triggers' in the left side bar

3. There should be one row in the table showing a trigger called 'VCS Trigger'. Click the drop-down menu option in the right-most column of this row, and select 'Disable trigger'.

4. Click 'Add New Trigger'. Choose 'Schedule Trigger' from the drop-down.

3. Configure the trigger as follows:

    When: Daily
    Time: 05:00
    Trigger only if there are pending changes: leave checked.

4. Save the trigger.


### Auto-start portal container on system startup

    wget https://bitbucket.org/qac-software/notes-and-scripts/raw/master/install-portal-server-service/install_portal_server_service.sh

    chmod +x install_portal_server_service.sh

    ./install_portal_server_service.sh

### Install Selenium grid hub & auto-start it on system startup

    wget https://bitbucket.org/qac-software/notes-and-scripts/raw/master/install-grid-hub-service/install_grid_hub_service.sh

    chmod +x install_grid_hub_service.sh

    ./install_grid_hub_service.sh
