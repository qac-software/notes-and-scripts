#!/bin/bash

cd ~

# download selenium grid jar 
wget http://selenium-release.storage.googleapis.com/3.11/selenium-server-standalone-3.11.0.jar

# download hub config json file
wget https://bitbucket.org/qac-software/notes-and-scripts/raw/master/install-grid-hub-service/selenium-hub-config.json


# download and install service
sudo wget -O /usr/lib/systemd/system/selenium_grid_hub.service https://bitbucket.org/qac-software/notes-and-scripts/raw/master/install-grid-hub-service/selenium_grid_hub.service
sudo systemctl enable selenium_grid_hub.service

echo "Service installed to /usr/lib/systemd/system/selenium_grid_hub.service"
echo ""
echo "Installation is complete, and the service will run on system start."

echo "More information and instructions are available in the README:"
echo "https://bitbucket.org/qac-software/notes-and-scripts/"
