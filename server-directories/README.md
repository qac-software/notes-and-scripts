# QAC Portal Server - Instance Directories

When a user starts a tool process, the server creates a temporary directory that acts as a sandbox for that single tool run; tools are expected to only write to this directory, and nowhere else. Instance directories are named directly after the instance ID, which is formatted as the name of the tool (from server's `config.json` file) followed by the current unix epoch timestamp in milliseconds (JavaScript `Date.now()`). An example of this is: `LINK_CHECKER_1523385999683`, where `LINK_CHECKER` is the tool name and `1523385999683` is the timestamp of when the instance was created.

The "master" directory that holds all these instance directories is configured by the node server configuration file. See the [Configuring node server](https://bitbucket.org/qac-software/notes-and-scripts/src/master/configuring-node-server/README.md) document for more details

When the server creates an instance directory, it will write a file called `config.toml` inside. Tools expect this file to exist, and expect to be given a path to the file as their first argument. Config data is generated as JSON by the portal server's frontend, passed to the backend, and written as TOML to the instance directory.

Inside `config.toml` is a property with the key "outputDir", under the TOML heading "pathing".

```toml
[pathing]
outputDir = "/path/to/instance/directory"
```

During the generation of the `config.toml` file, the server sets this pathing property to the same instance directory that the file is being written to. The tools expect the outputDir property to exist, and will use it as their working directory.

Apart from the `config.toml` file, the contents of the instance directory will be entirely up to the tools. They may create temporary files, store data, write logs, etc.

A pattern that is often expected from the tools is for each execution to output a `.zip` file (with any name) to their output directory when they've finished executing, containing any execution results they may have to offer (logs, reports, etc). Once an execution completes, the user who initiated it will be shown a button which, when clicked, will provide them with a download of this output `.zip` file.

### Cleanup

Tool instance directories are queued for deletion after a tool process has finished. This is done to avoid the accumulation of a large number of directories whose contents no longer matter. The timeouts (as of writing this document) are as follows:

**Tool finished running:**

30 minutes until deletion

**Tool finished running, *and* a user downloaded the results `.zip` file:**

15 minutes until deletion (overrides normal 'Tool finished running' timeout)

Tool directories are also cleaned up every time the portal node.js server starts. The server reads the creation date of every instance directory, and removes any that are older than 1 hour. In the current TeamCity configuration, the node server is restarted automatically early every morning to allow this cleanup to happen.

### Example Directory

Below is an example of how an instance directory for the link checker tool might be laid out after an execution.

LINK_CHECKER_123456/
    - [date]
      - [time]
        - logs
          - Errors.txt
          - Ledger.txt
          - Summary.txt
    - link-checker-1.1.1-[date][time].zip
